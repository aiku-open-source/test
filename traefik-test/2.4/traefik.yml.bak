global:
  #定期检查是否已发布新版本。（默认值：true）
  checkNewVersion: true
  #定期发送匿名使用情况统计信息。如果未指定该选项，则默认情况下将启用它。（默认值：false）
  sendAnonymousUsage: true
#serversTransport:
#  #禁用SSL证书验证。（默认值：false）
#  insecureSkipVerify: true
#  #添加用于自签名证书的证书文件。
#  rootCAs:
#    - foobar
#    - foobar
#  #如果不为零，则控制最大空闲（保持活动状态）以保留每个主机。如果为零，DefaultMaxIdleConnsPerHost被使用（默认值：200）
#  maxIdleConnsPerHost: 42
#  forwardingTimeouts:
#    #建立与后端服务器的连接之前要等待的时间。如果为零，则不存在超时。（默认值：30）
#    dialTimeout: 42
#    #完全写入请求（包括请求的正文，如果有）后等待服务器的响应标头的时间。如果为零，则不存在超时。（默认值：0）
#    responseHeaderTimeout: 42
#    #最长期限为其中一个空闲HTTP保持活动连接将保持打开关闭本身之前（默认值：90）
#    idleConnTimeout: 42
#entryPoints:
#  web:
#    #入口点地址。
#    address: 80
#  websecure:
#    #入口点地址。
#    address: 443
#    transport:
#      lifeCycle:
#        #在Traefik启动正常关闭程序之前保持接受请求的持续时间。（默认值：0）
#        requestAcceptGraceTimeout: 42
#        #在Traefik停止之前，给予主动请求的机会完成的时间。（默认值：10）
#        graceTimeOut: 42
#      respondingTimeouts:
#        #ReadTimeout是读取整个请求（包括正文）的最大持续时间。如果为零，则不设置超时。（默认值：0）
#        readTimeout: 42
#        #WriteTimeout是超时写入响应之前的最大持续时间。如果为零，则不设置超时。（默认值：0）
#        writeTimeout: 42
#        #IdleTimeout是空闲（保持活动状态）连接在关闭自身之前将保持空闲状态的最长时间。如果为零，则不设置超时。（默认值：180）
#        idleTimeout: 42
#    proxyProtocol:
#      #信任所有人。（默认值：false）
#      insecure: true
#      #仅信任来自选定IP的转发报头。
#      trustedIPs:
#        - foobar
#        - foobar
#    forwardedHeaders:
#      #信任所有转发的标头。（默认值：false）
#      insecure: true
#      #仅信任来自选定IP的转发报头。
#      trustedIPs:
#        - foobar
#        - foobar
#    http:
#      redirections:
#        entryPoint:
#          #重定向的目标入口点。
#          to: foobar
#          #用于重定向的方案。（默认值：https）
#          scheme: foobar
#          #应用永久重定向。（默认值：true）
#          permanent: true
#          #生成的路由器的优先级。（默认值：2147483646）
#          priority: 42
#      #链接到入口点的路由器的默认中间件。
#      middlewares:
#        - foobar
#        - foobar
#      tls:
#        #链接到入口点的路由器的默认TLS选项。
#        options: foobar
#        #链接到入口点的路由器的默认证书解析器。
#        certResolver: foobar
#        #链接到入口点的路由器的默认TLS域。
#        domains:
#            #默认主题名称。
#          - main: foobar
#            #主题备用名称。
#            sans:
#              - foobar
#              - foobar
#            #默认主题名称。
#          - main: foobar
#            #主题备用名称。
#            sans:
#              - foobar
#              - foobar
providers:
#  #后端限制持续时间：应用新配置之前，提供商提供的2个事件之间的最短持续时间。如果在短时间内发送多个事件，则可以避免不必要的重载。（默认值：2）
#  providersThrottleDuration: 42
  docker:
#    #约束是Traefik与容器标签匹配的表达式，以确定是否为该容器创建任何路线。
#    constraints: foobar
    #观看Docker Swarm事件。（默认值：true）
#    watch: true
    #Docker服务器端点。可以是tcp或UNIX套接字端点。（默认值：unix:///var/run/docker.sock）
    endpoint: "unix:///var/run/docker.sock"
    #默认规则。（默认值：Host(`{{ normalize .Name }}`)）
#    defaultRule: "Host(`{{ normalize.Name }}.docker.whoami`)"
#    tls:
#      #TLS CA
#      ca: foobar
#      #TLS CA.Optional（缺省：false）
#      caOptional: true
#      #TLS证书
#      cert: foobar
#      #TLS密钥
#      key: foobar
#      #TLS不安全跳过验证（缺省：false）
#      insecureSkipVerify: true
#    #默认情况下，暴露容器。（默认值：true）
#    exposedByDefault: true
#    #使用绑定端口的IP地址，而不是内部网络的IP地址。（默认值：false）
#    useBindPortIP: true
#    #在Swarm模式下使用Docker。（默认值：false）
#    swarmMode: true
#    #默认Docker网络。
#    network: "test"
#    #群模式的轮询间隔。（默认值：15）
#    swarmModeRefreshSeconds: 42
#    #HTTP连接的客户端超时。（默认值：0）
#    httpClientTimeout: 42
#  file:
#    #从目录中的一个或多个.toml或.yml文件加载动态配置。
#    directory: foobar
#    #观看提供商。（默认值：true）
#    watch: true
#    #从文件加载动态配置。
#    filename: foobar
#    #启用生成的配置模板的调试日志记录。（默认值：false）
#    debugLogGeneratedTemplate: true
#  marathon:
#    #约束是Traefik与应用程序标签匹配的表达式，以确定是否为该应用程序创建任何路由。
#    constraints: foobar
#    #示其他提供程序日志。（默认值：false）
#    trace: true
#    #观看提供商。（默认值：true）
#    watch: true
#    #拉松服务器端点。您也可以为Marathon指定多个端点。（默认值：http://127.0.0.1:8080）
#    endpoint: foobar
#    #默认规则。（默认值：Host(`{{ normalize .Name }}`)）
#    defaultRule: foobar
#    #默认情况下公开Marathon应用。（默认值：true）
#    exposedByDefault: true
#    #DCOS环境中的DCOSToken，它将覆盖Authorization标头。
#    dcosToken: foobar
#    tls:
#      #TLS CA
#      ca: foobar
#      #TLS CA.Optional（缺省：false）
#      caOptional: true
#      #TLS证书
#      cert: foobar
#      #TLS密钥
#      key: foobar
#      #TLS不安全跳过验证（缺省：false）
#      insecureSkipVerify: true
#    #设置Marathon的拨号程序超时。（默认值：5）
#    dialerTimeout: 42
#    #设置Marathon的响应标头超时。（默认值：60）
#    responseHeaderTimeout: 42
#    #为Marathon设置TLS握手超时。（默认值：5）
#    tlsHandshakeTimeout: 42
#    #设置TCP保持活动时间。（默认值：10）
#    keepAlive: 42
#    #强制使用任务的主机名。（默认值：false）
#    forceTaskHostname: true
#    basic:
#      #基本身份验证用户。
#      httpBasicAuthUser: foobar
#      #基本身份验证密码。
#      httpBasicPassword: foobar
#    #在部署过程中使用不成功的准备情况检查筛选出任务。（默认值：false）
#    respectReadinessChecks: true
#  kubernetesIngress:
#    #Kubernetes服务器端点（外部集群客户端必需）。
#    endpoint: foobar
#    #Kubernetes承载令牌（集群内客户端不需要）。
#    token: foobar
#    #Kubernetes证书颁发机构文件路径（集群内客户端不需要）。
#    certAuthFilePath: foobar
#    #Kubernetes命名空间
#    namespaces:
#      - foobar
#      - foobar
#    #使用的 Kubernetes Ingress标签选择器。
#    labelSelector: foobar
#    #要注意的kubernetes.io/ingress.class批注的值。
#    ingressClass: foobar
#    #入口刷新油门持续时间（默认值：0）
#    throttleDuration: 42s
#    ingressEndpoint:
#      #用于Kubernetes Ingress端点的IP。
#      ip: foobar
#      #用于Kubernetes Ingress端点的主机名。
#      hostname: foobar
#      #发布Kubernetes服务以复制状态。
#      publishedService: foobar
#  kubernetesCRD:
#    #Kubernetes服务器端点（外部集群客户端必需）。
#    endpoint: foobar
#    #Kubernetes承载令牌（集群内客户端不需要）
#    token: foobar
#    #Kubernetes证书颁发机构文件路径（集群内客户端不需要）
#    certAuthFilePath: foobar
#    #Kubernetes命名空间
#    namespaces:
#      - foobar
#      - foobar
#    #允许跨名称空间资源引用。（默认值：true）
#    allowCrossNamespace: true
#    #Kubernetes标签选择器。
#    labelSelector: foobar
#    #要注意的kubernetes.io/ingress.class批注的值。
#    ingressClass: foobar
#    #入口刷新油门持续时间（默认值：0）
#    throttleDuration: 42s
#  kubernetesGateway:
#    #Kubernetes服务器端点（外部集群客户端必需）。
#    endpoint: foobar
#    token: foobar
#    #Kubernetes证书颁发机构文件路径（集群内客户端不需要）。
#    certAuthFilePath: foobar
#    #Kubernetes命名空间
#    namespaces:
#      - foobar
#      - foobar
#    #Kubernetes标签选择器以选择特定的GatewayClass。
#    labelSelector: foobar
#    #Kubernetes刷新油门持续时间（默认值：0）
#    throttleDuration: 42s
#  rest:
#    #直接在名为traefik的entryPoint上激活REST Provider。（默认值：false）
#    insecure: true
#  rancher:
#    #约束是Traefik与容器标签匹配的表达式，以确定是否为该容器创建任何路线。
#    constraints: foobar
#    #观看提供商。（默认值：true）
#    watch: true
#    #默认规则。（默认值：Host(`{{ normalize .Name }}`)）
#    defaultRule: foobar
#    #默认情况下，暴露容器。（默认值：true）
#    exposedByDefault: true
#    #筛选状态为不健康和不活动的服务。（默认值：true）
#    enableServiceHealthFilter: true
#    #定义轮询间隔（以秒为单位）。（默认值：15）
#    refreshSeconds: 42
#    #每隔“ rancher.refreshseconds”（不太准确）轮询Rancher元数据服务。（默认值：false）
#    intervalPoll: true
#    #用于访问Rancher元数据服务的前缀。（默认值：latest）
#    prefix: foobar
#  consulCatalog:
#    #约束是Traefik与容器标签匹配的表达式，以确定是否为该容器创建任何路线。
#    constraints: foobar
#    #领事服务标签的前缀。默认'traefik'（默认值：traefik）
#    prefix: foobar
#    #检查Consul API的时间间隔。默认15秒（默认值：15）
#    refreshInterval: 42s
#    #强制读取完全一致。（默认值：false）
#    requireConsistent: true
#    #对目录读取使用陈旧的一致性。（默认值：false）
#    stale: true
#    #使用本地代理缓存进行目录读取。（默认值：false）
#    cache: true
#    #默认情况下，暴露容器。（默认值：true）
#    exposedByDefault: true
#    #默认规则。（默认值：Host(`{{ normalize .Name }}`)）
#    defaultRule: foobar
#    endpoint:
#      #领事服务器的地址（默认：127.0.0.1:8500）
#      address: foobar
#      #Consul服务器的URI方案
#      scheme: foobar
#      #要使用的数据中心。如果未提供，则使用默认代理数据中心
#      datacenter: foobar
#      #令牌用于提供按请求的ACL令牌，该令牌将覆盖代理的默认令牌
#      token: foobar
#      #WaitTime限制手表将阻塞多长时间。如果没有提供，所述试剂默认值将被使用（缺省：0）
#      endpointWaitTime: 42s
#      tls:
#        #TLS CA
#        ca: foobar
#        #TLS CA.Optional（缺省：false）
#        caOptional: true
#        #TLS证书
#        cert: foobar
#        #TLS密钥
#        key: foobar
#        #TLS不安全跳过验证（缺省：false）
#        insecureSkipVerify: true
#      httpAuth:
#        #基本身份验证用户名
#        username: foobar
#        #基本身份验证密码
#        password: foobar
#  ecs:
#    #约束是Traefik与容器标签匹配的表达式，以确定是否为该容器创建任何路线。
#    constraints: foobar
#    #默认情况下，公开服务（默认值：true）
#    exposedByDefault: true
#    #轮询间隔（秒）（默认值：15）
#    refreshSeconds: 42
#    #默认规则。（默认值：Host(`{{ normalize .Name }}`)）
#    defaultRule: foobar
#    #ECS群集名称（默认值：default）
#    clusters:
#      - foobar
#      - foobar
#    #自动发现集群（默认值：false）
#    autoDiscoverClusters: true
#    #用于请求的AWS区域
#    region: foobar
#    #用于发出请求的AWS凭证访问密钥
#    accessKeyID: foobar
#    #用于发出请求的AWS凭证访问密钥
#    secretAccessKey: foobar
#  consul:
#    #根密钥用于KV存储（默认值：traefik）
#    rootKey: foobar
#    #KV卖场终端（默认值：127.0.0.1:8500）
#    endpoints:
#      - foobar
#      - foobar
#    #KV用户名
#    username: foobar
#    #KV密码
#    password: foobar
#    tls:
#      #TLS CA
#      ca: foobar
#      #TLS CA.Optional（缺省：false）
#      caOptional: true
#      #TLS证书
#      cert: foobar
#      #TLS密钥
#      key: foobar
#      #TLS不安全跳过验证（缺省：false）
#      insecureSkipVerify: true
#  etcd:
#    #根密钥用于KV存储（默认值：traefik）
#    rootKey: foobar
#    #KV卖场终端（默认值：127.0.0.1:2379）
#    endpoints:
#      - foobar
#      - foobar
#    #KV用户名
#    username: foobar
#    #KV密码
#    password: foobar
#    tls:
#      #TLS CA
#      ca: foobar
#      #TLS CA.Optional（缺省：false）
#      caOptional: true
#      #TLS证书
#      cert: foobar
#      #TLS密钥
#      key: foobar
#      #TLS不安全跳过验证（缺省：false）
#      insecureSkipVerify: true
#  zooKeeper:
#    #根密钥用于KV存储（默认值：traefik）
#    rootKey: foobar
#    #KV卖场终端（默认值：127.0.0.1:2379）
#    endpoints:
#      - foobar
#      - foobar
#    #KV用户名
#    username: foobar
#    #KV密码
#    password: foobar
#    tls:
#      #TLS CA
#      ca: foobar
#      #TLS CA.Optional（缺省：false）
#      caOptional: true
#      #TLS证书
#      cert: foobar
#      #TLS密钥
#      key: foobar
#      #TLS不安全跳过验证（缺省：false）
#      insecureSkipVerify: true
#  redis:
#    #根密钥用于KV存储（默认值：traefik）
#    rootKey: foobar
#    #KV卖场终端（默认值：127.0.0.1:2379）
#    endpoints:
#      - foobar
#      - foobar
#    #KV用户名
#    username: foobar
#    #KV密码
#    password: foobar
#    tls:
#      #TLS CA
#      ca: foobar
#      #TLS CA.Optional（缺省：false）
#      caOptional: true
#      #TLS证书
#      cert: foobar
#      #TLS密钥
#      key: foobar
#      #TLS不安全跳过验证（缺省：false）
#      insecureSkipVerify: true
#  http:
#    #从此端点加载配置。
#    endpoint: foobar
#    #端点的轮询间隔。（默认值：5）
#    pollInterval: 42
#    #端点的轮询超时。（默认值：5）
#    pollTimeout: 42
#    tls:
#      #TLS CA
#      ca: foobar
#      #TLS CA.Optional（缺省：false）
#      caOptional: true
#      #TLS证书
#      cert: foobar
#      #TLS密钥
#      key: foobar
#      #TLS不安全跳过验证（缺省：false）
#      insecureSkipVerify: true
api:
  #直接在名为traefik的entryPoint上激活API。（默认值：false）
  insecure: true
  #激活仪表板。（默认值：true）
  dashboard: true
  #启用其他端点进行调试和配置。（默认值：false）
  debug: true
#metrics:
#  prometheus:
#    #用于等待时间指标的存储桶。（默认值：0.100000, 0.300000, 1.200000, 5.000000）
#    buckets:
#      - 42
#      - 42
#    #在入口点上启用指标。（默认值：true）
#    addEntryPointsLabels: true
#    #启用服务指标。（默认值：true）
#    addServicesLabels: true
#    #入口点（默认值：traefik）
#    entryPoint: foobar
#    #手动布线（默认值：false）
#    manualRouting: true
#  datadog:
#    #Datadog的地址。（默认值：whoami:8125）
#    address: foobar
#    #Datadog推送间隔。（默认值：10）
#    pushInterval: 42
#    #在入口点上启用指标。（默认值：true）
#    addEntryPointsLabels: true
#    #启用服务指标。（默认值：true）
#    addServicesLabels: true
#  #StatsD指标导出器类型。（默认值：false）
#  statsD:
#    #StatsD地址。（默认值：whoami:8125）
#    address: foobar
#    #StatsD推送间隔。（默认值：10）
#    pushInterval: 42
#    #在入口点上启用指标。（默认值：true）
#    addEntryPointsLabels: true
#    #启用服务指标。（默认值：true）
#    addServicesLabels: true
#    #用于度量标准收集的前缀。（默认值：traefik）
#    prefix: foobar
#  #InfluxDB指标导出器类型。（默认值：false）
#  influxDB:
#    #InfluxDB地址。（默认值：whoami:8089）
#    address: foobar
#    #InfluxDB地址协议（udp或http）。（默认值：udp）
#    protocol: foobar
#    #InfluxDB推送间隔。（默认值：10）
#    pushInterval: 42
#    #协议为http时使用的InfluxDB数据库。
#    database: foobar
#    #协议为http时使用的InfluxDB保留策略。
#    retentionPolicy: foobar
#    #InfluxDB用户名（仅用于http）。
#    username: foobar
#    #InfluxDB密码（仅适用于http）。
#    password: foobar
#    #在入口点上启用指标。（默认值：true）
#    addEntryPointsLabels: true
#    #启用服务指标。（默认值：true）
#    addServicesLabels: true
ping:
  #入口点（默认值：traefik）
  entryPoint: "traefik"
#  #手动布线（默认值：false）
#  manualRouting: true
#  #终止状态代码（默认值：503）
#  terminatingStatusCode: 42
log:
  #日志级别设置为traefik日志。（默认值：ERROR）
  level: "ERROR"
#  #Traefik日志文件路径。如果省略或为空，则使用Stdout
#  filePath: foobar
#  #Traefik日志格式：json | 常见（缺省：common）
#  format: foobar
##访问日志设置。（默认值：false）
#accessLog:
#  #访问日志文件路径。如果省略或为空，则使用Stdout。
#  filePath: foobar
#  #访问日志格式：json | 常见（缺省：common）
#  format: foobar
#  filters:
#    #将访问日志的状态码保留在指定范围内。
#    statusCodes:
#      - foobar
#      - foobar
#    #至少进行一次重试时，请保留访问日志。（默认值：false）
#    retryAttempts: true
#    #保留请求时间超过指定持续时间的访问日志。（默认值：0）
#    minDuration: 42
#  fields:
#    #字段的默认模式：keep | 降（缺省：keep）
#    defaultMode: foobar
#    names:
#      #字段的替代模式
#      name0: foobar
#      name1: foobar
#    headers:
#      #字段的默认模式：keep | 下降 纂（缺省：drop）
#      defaultMode: foobar
#      names:
#        #标题的替代模式
#        name0: foobar
#        name1: foobar
#  #要以缓冲方式处理的访问日志行数。（默认值：0）
#  bufferingSize: 42
##OpenTracing配置。（默认值：false）
#tracing:
#  #设置此服务的名称。（默认值：traefik）
#  serviceName: foobar
#  #设置跨度名称的最大字符数限制（默认0 =无限制）。（默认值：0）
#  spanNameLimit: 42
#  #Jaeger的设置。（默认值：false）
#  jaeger:
#    #设置采样服务器网址。（默认值：http://localhost:5778/sampling）
#    samplingServerURL: foobar
#    #设置采样类型。（默认值：const）
#    samplingType: foobar
#    #设置采样参数。（默认值：1.000000）
#    samplingParam: 42
#    #设置报告者将使用的jaeger-agent的host：port。（默认值：127.0.0.1:6831）
#    localAgentHostPort: foobar
#    #生成128位跨度ID。（默认值：false）
#    gen128Bit: true
#    #要使用的传播格式（jaeger / b3）。（默认值：jaeger）
#    propagation: foobar
#    #设置要用于trace-id的标头。（默认值：uber-trace-id）
#    traceContextHeaderName: foobar
#    #禁用代理的主机名的定期重新解析，如果发生更改，则重新连接。（默认值：true）
#    disableAttemptReconnecting: true
#    collector:
#      #指示记者通过此URL将跨度发送到jaeger-collector。
#      endpoint: foobar
#      #将跨度发送到jaeger-collector时用于基本http认证的用户。
#      user: foobar
#      #将跨度发送到jaeger-collector时用于基本http认证的密码。
#      password: foobar
#  #Zipkin的设置。（默认值：false）
#  zipkin:
#    #向其报告跟踪的HTTP端点。（默认值：http://localhost:9411/api/v2/spans）
#    httpEndpoint: foobar
#    #使用Zipkin SameSpan RPC样式跟踪。（默认值：false）
#    sameSpan: true
#    #使用Zipkin 128位根跨度ID。（默认值：true）
#    id128Bit: true
#    #跟踪请求的0.0到1.0之间的比率。（默认值：1.000000）
#    sampleRate: 42
#  #Datadog的设置。（默认值：false）
#  datadog:
#    #设置报告程序将使用的datadog-agent的host：port。（默认值：whoami:8126）
#    localAgentHostPort: foobar
#    #要在所有跨度上设置的键：值标签。
#    globalTag: foobar
#    #启用Datadog调试。（默认值：false）
#    debug: true
#    #启用优先采样。使用分布式跟踪时，必须启用此选项才能对分布式跟踪的所有部分进行采样。（默认值：false）
#    prioritySampling: true
#    #指定将用于存储跟踪ID的标头名称。
#    traceIDHeaderName: foobar
#    #指定将用于存储父ID的标头名称。
#    parentIDHeaderName: foobar
#    #指定将用于存储采样优先级的标头名称。
#    samplingPriorityHeaderName: foobar
#    #指定标头名称前缀，该标头名称前缀将用于在地图中存储行李物品。
#    bagagePrefixHeaderName: foobar
#  #Instana的设置。（默认值：false）
#  instana:
#    #设置报告者将使用的instana-agent的主机。
#    localAgentHost: foobar
#    #设置报告程序将使用的instana-agent的端口。（默认值：42699）
#    localAgentPort: 42
#    #设置instana-agent的日志级别。（ '错误'， '警告'， '信息'， '调试'）（默认：info）
#    logLevel: foobar
#  #干草堆的设置。（默认值：false）
#  haystack:
#    #设置报告程序将使用的haystack-agent的主机。（默认值：127.0.0.1）
#    localAgentHost: foobar
#    #设置报告程序将使用的haystack-agent的端口。（默认值：35000）
#    localAgentPort: 42
#    #要在所有跨度上设置的键：值标签。
#    globalTag: foobar
#    #指定将用于存储跟踪ID的标头名称。
#    traceIDHeaderName: foobar
#    #指定将用于存储父ID的标头名称。
#    parentIDHeaderName: foobar
#    #指定将用于存储跨度ID的标题名称。
#    spanIDHeaderName: foobar
#    #指定标头名称前缀，该标头名称前缀将用于在地图中存储行李物品。
#    baggagePrefixHeaderName: foobar
#  #弹性设置。（默认值：false）
#  elastic:
#    #设置Elastic APM服务器的URL。
#    serverURL: foobar
#    #设置用于连接到Elastic APM Server的令牌
#    secretToken: foobar
#    #设置Traefik部署在其中的环境的名称，例如“生产”或“登台”。
#    serviceEnvironment: foobar
##启用CNAME拼合。（默认值：false
#hostResolver:
#  #一个标志，以启用/禁用CNAME平坦化（缺省：false）
#  cnameFlattening: true
#  #用于DNS解析的resolv.conf（缺省：/etc/resolv.conf）
#  resolvConfig: foobar
#  #DNS的最大深度递归拆分（缺省：5）
#  resolvDepth: 42
#certificatesResolvers:
#  #证书解析器配置。（默认值：false）
#  CertificateResolver0:
#    acme:
#      #用于注册的电子邮件地址。
#      email: foobar
#      #要使用的CA服务器。（默认值：https://acme-v02.api.letsencrypt.org/directory）
#      caServer: foobar
#      #首选使用的链条。
#      preferredChain: foobar
#      #要使用的存储空间。（默认值：acme.json）
#      storage: foobar
#      #用于生成证书私钥的KeyType。允许值'EC256'，'EC384'，'RSA2048'，'RSA4096'，'RSA8192'。（默认值：RSA4096）
#      keyType: foobar
#      eab:
#        #来自外部CA的密钥标识符。
#        kid: foobar
#        #来自外部CA的Base64编码的HMAC密钥。
#        hmacEncoded: foobar
#      #激活DNS-01质询。（默认值：false
#      dnsChallenge:
#        #使用基于DNS-01的质询提供程序，而不是HTTPS。
#        provider: foobar
#        #假设DNS在延迟几秒钟后传播，而不是查找和查询名称服务器。（默认值：0）
#        delayBeforeCheck: 42
#        #使用以下DNS服务器来解析FQDN权限。
#        resolvers:
#          - foobar
#          - foobar
#        #在通知ACME DNS挑战准备就绪之前，请禁用DNS传播检查。[不推荐]（缺省：false）
#        disablePropagationCheck: true
#      #激活HTTP-01挑战。（默认值：false）
#      httpChallenge:
#        #HTTP挑战EntryPoint
#        entryPoint: foobar
#      #激活TLS-ALPN-01挑战。（默认值：true）
#      tlsChallenge: {}
#  CertificateResolver1:
#    acme:
#      #用于注册的电子邮件地址。
#      email: foobar
#      #要使用的CA服务器。（默认值：https://acme-v02.api.letsencrypt.org/directory）
#      caServer: foobar
#      #首选使用的链条。
#      preferredChain: foobar
#      #要使用的存储空间。（默认值：acme.json）
#      storage: foobar
#      #用于生成证书私钥的KeyType。允许值'EC256'，'EC384'，'RSA2048'，'RSA4096'，'RSA8192'。（默认值：RSA4096）
#      keyType: foobar
#      eab:
#        #来自外部CA的密钥标识符。
#        kid: foobar
#        #来自外部CA的Base64编码的HMAC密钥。
#        hmacEncoded: foobar
#      #激活DNS-01质询。（默认值：false
#      dnsChallenge:
#        #使用基于DNS-01的质询提供程序，而不是HTTPS。
#        provider: foobar
#        #假设DNS在延迟几秒钟后传播，而不是查找和查询名称服务器。（默认值：0）
#        delayBeforeCheck: 42
#        #使用以下DNS服务器来解析FQDN权限。
#        resolvers:
#          - foobar
#          - foobar
#        #在通知ACME DNS挑战准备就绪之前，请禁用DNS传播检查。[不推荐]（缺省：false）
#        disablePropagationCheck: true
#      #激活HTTP-01挑战。（默认值：false）
#      httpChallenge:
#        #HTTP挑战EntryPoint
#        entryPoint: foobar
#      #激活TLS-ALPN-01挑战。（默认值：true）
#      tlsChallenge: {}
#pilot:
#  #Traefik飞行员令牌。
#  token: foobar
#experimental:
#  #允许使用Kubernetes网关api提供程序。（默认值：false）
#  kubernetesGateway: true
#  plugins:
#    Descriptor0:
#      #插件的模块名称。
#      moduleName: foobar
#      #插件的版本。
#      version: foobar
#    Descriptor1:
#      #插件的模块名称。
#      moduleName: foobar
#      #插件的版本。
#      version: foobar
#  devPlugin:
#    #插件的GOPATH。
#    goPath: foobar
#    #插件的模块名称。
#    moduleName: foobar